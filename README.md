<!--
SPDX-FileCopyrightText: 2023 David Runge <dvzrv@archlinux.org>
SPDX-License-Identifier: CC-BY-SA-4.0
-->

# archlinux.pacman

An [Ansible](https://www.ansible.com/) role to configure certain aspects of [pacman](https://gitlab.archlinux.org/pacman/pacman/), such as database and package cache cleanup and the initialization of [pacman-key](https://man.archlinux.org/man/pacman-key.8).
The `paccache.timer` systemd timer unit for [paccache](https://man.archlinux.org/man/paccache.8) from the [pacman-contrib](https://gitlab.archlinux.org/pacman/pacman-contrib) package can be enabled to regularly purge unused packages from the cache.

## Requirements

An Arch Linux system or an Arch Linux chroot.

## Role Variables

This role relies on the following variables to configure `pacman`, `pacman-contrib` and `reflector`.
Dicts are unset by default. If they are provided only partially, their remaining fields are set using the below defaults.

* `pacman`: dict to define settings for pacman (unset by default)
  * `force_clear_databases`: whether to clear the package databases using a handler (defaults to `false`)
  * `force_clear_cache`: whether to clear the package cache using a handler (defaults to `false`)
  * `force_clear_keyring`: whether to remove `/etc/pacman.d/gnupg/` using a handler (defaults to `false`)
  * `mirrorlist_country`: the country code for which to retrieve a basic mirrorlist from the website (defaults to `all`)
  * `mirrorlist_prefetch`: whether to prefetch a basic mirrorlist from the website (defaults to `false`) - useful when bootstrapping
  * `mirrorlist_url`: the base URL for retrieving a basic mirrorlist from the website (defaults to `https://archlinux.org/mirrorlist/`)
  * `upgrade`: a boolean value indicating whether to upgrade the system after setting up pacman (defaults to `true`)

* `pacman_contrib`: whether to install `pacman-contrib` and activate `paccache.timer` (defaults to `false`)
  * `paccache`: whether to activate and start `paccache.timer` (defaults to `false`)

* `reflector`: dict to define settings for configuring `reflector`. Only used if at least one field is defined. Missing fields are merged with defaults.
  * `url`: the URL from where to retrieve data (defaults to `https://www.archlinux.org/mirrors/status/json/`)
  * `country`: the country for which to retrieve data (defaults to `Germany`)
  * `age`: the age parameter (defaults to `5`)
  * `latest`: the latest parameter (defaults to `30`)
  * `number`: the number parameter (defaults to `20`)
  * `sort`: the sort parameter (defaults to `rate`)
  * `save`: where to save the mirrorlist file (defaults to `/etc/pacman.d/mirrorlist`)
  * `threads`: how many threads to use (defaults to `5`)
  * `protocols`: for which protocols to retrieve mirrors (defaults to `[https, http, rsync]`)

The following variables have no defaults in this role, but are available to override behavior and use special functionality:
* `chroot_dir`: the directory to chroot into, when including `chroot.yml` tasks (defaults to unset)

## Dependencies

None

## Example Playbook

```yaml
- name: Force remove sync databases, get custom mirrorlist for France and activate paccache.timer
  hosts: my_hosts
  vars:
    pacman:
      force_clear_database: true
    pacman_contrib:
      paccache: true
    reflector:
      country: France
  tasks:
    - name: Include archlinux.pacman
      ansible.builtin.include_role:
        name: archlinux.pacman
```

## License

GPL-3.0-or-later

## Author Information

David Runge <dvzrv@archlinux.org>
